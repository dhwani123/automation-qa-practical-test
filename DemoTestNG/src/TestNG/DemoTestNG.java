package TestNG;

import org.testng.annotations.Test;

import java.util.ArrayList;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;

import pages.OpenHomePage;
import pages.SearchMIproductsPage;
import pages.ShareOnFacebookPage;
import pages.GetEMIamountPage;
import pages.GetFacebookTitlePage;
import pages.ProductDescriptionPage;
import pages.SetPriceRangePage;
import pages.SortPricePage;
import pages.VerifySortingPage;

@Test
public class DemoTestNG {

	public WebDriver driver;

	@BeforeMethod
	public void before_method() {
		// launch the chrome browser and open the application url
		String exePath = "C:\\Users\\dhwani.patel\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();

	}
	
	public void flipkartLogin() throws InterruptedException {
		OpenHomePage objOpenHomePage = new OpenHomePage(driver);
		SearchMIproductsPage objSearchMIProduct = new SearchMIproductsPage(driver);
		SetPriceRangePage objSetPricerange = new SetPriceRangePage(driver);
		SortPricePage objSortRange = new SortPricePage(driver);
		VerifySortingPage objVerifySorting = new VerifySortingPage(driver);
		ProductDescriptionPage objProductDescription = new ProductDescriptionPage(driver);
		ShareOnFacebookPage objShareOnFb = new ShareOnFacebookPage(driver);
		GetFacebookTitlePage objFacebookTitle = new GetFacebookTitlePage(driver);
		GetEMIamountPage objEMIamt = new GetEMIamountPage(driver);
		objOpenHomePage.flipkartPage();
		objSearchMIProduct.searchMI(driver).click();
		objSearchMIProduct.searchMI(driver).sendKeys("mi");	
		objSearchMIProduct.searchButton().click();
		objSetPricerange.pickCatagory().click();
		objSetPricerange.slideRange();
		objSortRange.sortLowToHigh().click();
		objVerifySorting.verifyPriceSorting();
		objProductDescription.openPdpPage().click();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		Thread.sleep(3000);
		driver.switchTo().window(tabs.get(1));
		Thread.sleep(3000);
		objProductDescription.verifyProductDescription();
		objShareOnFb.clickOnShare().click();
		objShareOnFb.clickFacebookIcon().click();
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		Thread.sleep(3000);
		objFacebookTitle.getFacebookTitle();
		driver.close();
		driver.switchTo().window(tabs1.get(1));
		Thread.sleep(3000);
		objEMIamt.getEMIAmount();
		objVerifySorting.getInterestAmount();
		driver.quit();
		
	}
	

}
