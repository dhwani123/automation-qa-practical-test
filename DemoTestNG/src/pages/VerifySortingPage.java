package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class VerifySortingPage {
	public WebDriver driver;

	public VerifySortingPage(WebDriver driver) {
		this.driver = driver;
	}

	int firstPrice;

	public void verifyPriceSorting() throws InterruptedException {
		for (int i = 2; true; i++) {
			WebElement priceAfterSorting1 = driver
					.findElement(By.xpath("//div[@id=\"container\"]/div/div[3]/div[2]/div/div[2]/div[" + i
							+ "]/div/div/div/a/div[2]/div[2]/div[1]/div/div[1]"));
			String getFirstPrice = priceAfterSorting1.getText();
			Thread.sleep(3000);
			System.out.println(getFirstPrice);
			getFirstPrice = getFirstPrice.substring(1);
			firstPrice = Integer.parseInt(getFirstPrice.replaceAll(",", ""));

			System.out.println("first price is: " + firstPrice);
			Thread.sleep(3000);

			WebElement priceAfterSorting2 = driver
					.findElement(By.xpath("//div[@id=\"container\"]/div/div[3]/div[2]/div/div[2]/div[" + (i + 1)
							+ "]/div/div/div/a/div[2]/div[2]/div[1]/div/div[1]"));
			String getSecondPrice = priceAfterSorting2.getText();
			getSecondPrice = getSecondPrice.substring(1);
			int secondPrice = Integer.parseInt(getSecondPrice.replaceAll(",", ""));
			System.out.println("second price is: " + secondPrice);

			if (firstPrice != secondPrice) {
				if (firstPrice < secondPrice) {
					System.out.println("Sorting worked perfectly!");
				} else {
					System.out.println("Sorting did not work!");
				}
				break;
			}
			Thread.sleep(3000);
		}
	}

	public void getInterestAmount() throws InterruptedException {
		int interest = 12; // Assume interest rate 12
		int years = 1;
		int emiPerYear;
		emiPerYear = (firstPrice * interest * years / 100);
		System.out.println("Interest amount as per EMI per year is: " + emiPerYear);
	}
}
