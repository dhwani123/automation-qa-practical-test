package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProductDescriptionPage {
	public WebDriver driver;
	By openPDPpage = By.xpath("//div[@id=\"container\"]/div/div[3]/div[2]/div[1]/div[2]/div[2]/div/div/div/a/div[2]/div[1]/div[1]");
	By verifyProductDesc = By.xpath("//div[@class=\"_2HVvN7\"]");
	
	public ProductDescriptionPage(WebDriver driver) {
		this.driver = driver;
	}
	private static WebElement element = null;
	
	public WebElement openPdpPage() throws InterruptedException {
			element = driver.findElement(openPDPpage);
			return element;
			
	}
	
	public WebElement verifyProductDescription() throws InterruptedException {
		element = driver.findElement(verifyProductDesc);
		return element;	
	}
	
	
}
