package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class SetPriceRangePage {
	public WebDriver driver;
	By pickFilterCatagory = By.xpath("//*[@id=\"container\"]/div/div[3]/div[2]/div[1]/div[1]/div/div/div/div/section/div[2]/div[2]/a[1]");
	By leftLocation = By.xpath("//div[@class='_3G9WVX oVjMho']");
	By rightLocation = By.xpath("//div[@class='_3G9WVX _2N3EuE']");
	
	public SetPriceRangePage(WebDriver driver){
        this.driver = driver;
    }
	
	private static WebElement element = null;
	
	public WebElement pickCatagory() throws InterruptedException {
			Thread.sleep(2000);
			element = driver.findElement(pickFilterCatagory);
			return element;		
	}
	
	public void slideRange() throws InterruptedException {
		Actions move = new Actions(driver);
		Thread.sleep(3000);
		WebElement leftLocation = driver.findElement(By.xpath("//div[@class='_3G9WVX oVjMho']"));
		Thread.sleep(1000);
		Action MinMove = move.dragAndDropBy(leftLocation, 25, 0).build();
		Thread.sleep(1000);
		MinMove.perform();
		Thread.sleep(1000);
		WebElement rightLocation = driver.findElement(By.xpath("//div[@class='_3G9WVX _2N3EuE']"));
		Thread.sleep(1000);
		Action MaxMove = move.dragAndDropBy(rightLocation, -75, 0).build();
		Thread.sleep(1000);
		MaxMove.perform();
		Thread.sleep(3000);
		
	}
	
}
