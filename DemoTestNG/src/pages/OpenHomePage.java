package pages;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;

public class OpenHomePage {
	public WebDriver driver;
	By loginRegisterPage = By.xpath("//div[@class=\"Og_iib col col-2-5 _3SWFXF\"]");
	By cancelPage = By.xpath("//button[@class=\"_2AkmmA _29YdH8\"]");
	
	public OpenHomePage(WebDriver driver){
        this.driver = driver;
    }
			 public void flipkartPage(){
					driver.get("https://www.flipkart.com/");
					driver.manage().window().maximize();
			        driver.findElement(loginRegisterPage);
			    	if (driver.findElement(loginRegisterPage).isDisplayed()) {
						driver.findElement(cancelPage);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						driver.findElement(cancelPage).click();
					}
					String expectedTitle = "Online Shopping Site for Mobiles, Electronics, Furniture, Grocery, Lifestyle, Books & More. Best Offers!";	    	
					    			String actualTitle = driver.getTitle();
			    			System.out.println(actualTitle);
			    	
			    			AssertJUnit.assertEquals(expectedTitle, actualTitle);
			    }
			 
}
