package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchMIproductsPage {
	public WebDriver driver;
	By searchKeyword = By.xpath("//div[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[2]/form/div/div/input");
	By searchButton = By.xpath("//*[@class=\"vh79eN\"]");
	
	public SearchMIproductsPage(WebDriver driver){
        this.driver = driver;
    }
	
	private static WebElement element = null;
	
	public WebElement searchMI(WebDriver driver) throws InterruptedException {
		element = driver.findElement(searchKeyword);
		return element;
	}
	
	public WebElement searchButton() throws InterruptedException {
		element = driver.findElement(searchButton);
		return element;
	}
}
