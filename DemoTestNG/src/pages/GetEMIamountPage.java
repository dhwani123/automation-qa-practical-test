package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GetEMIamountPage {
	public WebDriver driver;
	By getEMIAmount = By.xpath("//li[@class=\\\"_2ptBxu\\\" and contains(text(), 'EMI')]");
	
	public GetEMIamountPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void getEMIAmount() {
		String getEMIAmt = driver.findElement(By.xpath("//li[@class=\"_2ptBxu\" and contains(text(), 'EMI')]")).getText();
		getEMIAmt = getEMIAmt.substring(27);
		String finalEMIAmt = getEMIAmt.split("/")[0];
		System.out.println("EMI per month is: " + finalEMIAmt);
	}
	

}
