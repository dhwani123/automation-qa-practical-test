package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ShareOnFacebookPage {
	public WebDriver driver;
	By clickShareBtn = By.xpath("//span[contains(text(), 'Share')]");
	By shareOnFacebook = By.xpath("//span[@class=\"iTS58f\" and contains(text(), 'Facebook')]");
	
	public ShareOnFacebookPage(WebDriver driver) {
		this.driver = driver;
	}
	
	private static WebElement element = null;
	
	public WebElement clickOnShare() throws InterruptedException {
		element = driver.findElement(clickShareBtn);
		Thread.sleep(3000);
		return element;	
	}
	
	public WebElement clickFacebookIcon() throws InterruptedException {
		element = driver.findElement(shareOnFacebook);
		Thread.sleep(3000);
		return element;
	}
	
	

}
