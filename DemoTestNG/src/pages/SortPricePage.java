package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SortPricePage {
	public WebDriver driver;
	By priceLowToHigh = By.xpath("//div[@id=\"container\"]/div/div[3]/div[2]/div/div[2]/div[1]/div/div/div[2]/div[3]");

	public SortPricePage(WebDriver driver){
        this.driver = driver;
    }
	
	private static WebElement element = null;
	
	public WebElement sortLowToHigh() throws InterruptedException {
		element = driver.findElement(priceLowToHigh);
        Thread.sleep(3000);
        return element;
		
	}
}
