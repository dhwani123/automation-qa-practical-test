package pages;

import java.util.Set;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GetFacebookTitlePage {
	public WebDriver driver;
	By clickShareBtn = By.xpath("//span[contains(text(), 'Share')]");
	
	public GetFacebookTitlePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void getFacebookTitle() throws InterruptedException {
		String parentWindow = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindow : allWindows) {
			if (!parentWindow.equalsIgnoreCase(childWindow)) {
				driver.switchTo().window(childWindow);
				Thread.sleep(6000);
				System.out.println(driver.getTitle());
			}
		}
	}

}
